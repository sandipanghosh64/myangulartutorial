import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SuccsessAlertComponent } from './succsess-alert/succsess-alert.component';
import { WarningAlertComponent } from './warning-alert/warning-alert.component';
import { FormComponent } from './form/form.component';
import { ParagraphComponent } from './paragraph/paragraph.component';
import { LogComponent } from './log/log.component';

@NgModule({
  declarations: [
    AppComponent,
    SuccsessAlertComponent,
    WarningAlertComponent,
    FormComponent,
    ParagraphComponent,
    LogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
