import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.scss']
})
export class LogComponent implements OnInit {

  id:number=0;
  timestamp:number=0;
  

  constructor() { 
    this.id=document.getElementsByTagName("app-log").length+1;
    this.timestamp=new Date().getTime();
  }

  ngOnInit() {
  }

  getColor(){
    return this.id > 4?'blue':'white';
  }

  getId(){
    return this.id;
  }

  }
