import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-paragraph',
  templateUrl: './paragraph.component.html',
  styleUrls: ['./paragraph.component.scss']
})
export class ParagraphComponent implements OnInit {

  showText:boolean=true;
  logsArray=[];

  constructor() { }

  ngOnInit() {
  }

  toggleText(){
    if(this.showText === true){
    this.showText=false;
    }else{
      this.showText=true;
    }
    this.addLogs();
  }

  addLogs(){
    this.logsArray.push("log");
  }



  
 
}
