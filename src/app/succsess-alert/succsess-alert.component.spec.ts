import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccsessAlertComponent } from './succsess-alert.component';

describe('SuccsessAlertComponent', () => {
  let component: SuccsessAlertComponent;
  let fixture: ComponentFixture<SuccsessAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccsessAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccsessAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
